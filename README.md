## DESAFIO 1
### Objetivo
Dar desafios práticos para habituar vocês ao Linux.

### Instruções do Desafio
1. Vocês devem criar um repositório com o nome: desafio-01-linux
2. Nesse repositório, vocês devem subir um arquivo markdown e todas as respostas devem estar nele. (https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
3. Para cada etapa, vocês devem adicionar o comando que vocês utilizaram e a saída do resultado obtido.

### Desafio
Passo 1 - Crie uma pasta chamada desafio-01-linux via linha de comando

Passo 2 - Crie um arquivo vazio chamado arquivo desafio-01-linux.txt via linha de comando

Passo 3 - Adicione a palavra “fee” no arquivo via linha de comando

Passo 4 - Visualize o conteúdo do arquivo criado via linha de comando

Passo 5 - Crie uma nova pasta chamada sub-pasta-desafio-01 via linha de comando

Passo 6 - Mova o arquivo desafio-01-linux.txt para a nova pasta criada via linha de comando

Passo 7 - Renomeie o arquivo para df-01-linux.txt via linha de comando

Passo 8 - Volte a pasta raiz e delete as pastas e arquivo criados via linha de comando

Passo 9 - Instale o pacote “vim” via linha de comando

Passo 10 - Descubra o nome da sua maquina via linha de comando

Passo 11 - Descubra o IP da sua maquina via linha de comando
